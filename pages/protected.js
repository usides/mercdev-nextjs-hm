import { useSession } from 'next-auth/client';
import Layout from '../components/Layout';

export default function Page() {
  const [session, loading] = useSession();

  if (typeof window !== 'undefined' && loading) return null;

  if (!session) {
    return (
      <Layout>
        <p className='noaccess'>Access Denied</p>
      </Layout>
    );
  }

  return (
    <Layout>
      <h1>Protected Page</h1>
      <p>Some secret data...</p>
    </Layout>
  );
}
