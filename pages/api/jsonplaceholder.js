export default async function handler(req, res) {
  const respond = await fetch(
    `https://jsonplaceholder.typicode.com/posts?_limit=3`,
  );
  const posts = await respond.json();

  res.status(200).json(posts);
}
