import Link from 'next/link';
import styles from '../styles/Nav.module.css';

const Nav = () => {
  return (
    <nav className={styles.nav}>
      <ul>
        <li>
          <Link href='/'>Home</Link>
        </li>
        <li>
          <Link href='/users'>Users</Link>
          <Link href='/protected'>Protected</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
